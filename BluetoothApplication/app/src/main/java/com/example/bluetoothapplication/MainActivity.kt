package com.example.bluetoothapplication

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import java.util.ArrayList

class MainActivity : AppCompatActivity() {

    lateinit var bl_adapter:BluetoothAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bl_adapter = BluetoothAdapter.getDefaultAdapter()

        if (bl_adapter.isEnabled){
            Log.i("Bluetooth", "Le bluetooth est allumé")
            Toast.makeText(this,"Le bluetooth est allumé", Toast.LENGTH_SHORT).show()
            makeDeviceDiscorable()
            getPairedDevices()
        } else {
            Log.i("Bluetooth", "BLUETOOTH IS OFF")
        }
    }

    fun makeDeviceDiscorable(){
        val intent = Intent(Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE))
        startActivityForResult(intent, 1)
    }

    fun getPairedDevices(){

        val devices = bl_adapter.bondedDevices
        val i = 0
        val devices_names = ArrayList<String>()
        for (device in devices){
            devices_names.add(device.name)
        }
        Log.i("Paired devices", devices_names.toString())

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            Activity.RESULT_OK -> Log.i("OK","Appareil visible")
            else -> Log.e("ERREUR", "Une erreur s'est produite")
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}